<html>
    <head>
        <title>Resume Tanilon</title>
        
    </head>
    <body>
            <?php echo "<link rel='stylesheet' type='text/css' href='main.css'/>" ?>
            <div id="section-left">
            <div class="section intro">
                <div class="logo"><img src="ako.png" alt="Jonard Tanilon" width="250" height="230"></div>
                
                <?php echo "<h1>Jonard Tanilon</h1>" ?>
                
                <hr>
                
                <div class="content">
                    <span class="intro">+63 948 083 4604</span><br><br>
                    <span class="email"><a href="jonardTanilon98@gmail.com">jonardTanilon98@gmail</span>
                </div>
                
                <hr>
                
                <div id="contact">
                    <span class="num">+63 948 083 4604 </span>
                    <br><br>
                    <div class="git"><a href="gitlab.com">GitLab</a></div>
                    <div class="email"><a href="https://plus.google.com/u/0/106671133982586269221">Gmail</a></div>
                    <div class="web"><a href="facebook.com/jonard.tanilon">Facebook</a></div>
                    <div class="insta"><a href="https://www.instagram.com/nardtanilon/">Instagram</a></div>
                    <div class="twit"><a href="twitter.com">Twitter</a></div>
                    
                </div>
            </div>
        </div>
        
        <div id="section-right">
            <div class="wrapper">
                <div class="section">

                    <div class="title"><img src="user.png" width="20" height="20">Profile</div>
                    <?php echo "<p><i>An aspiring 3<sup>rd</sup> year BSIT student at USeP who wants to excell in the field of
                    Information Technology</i><hr> <b>Address:</b> Prk. 2 - Durian, Apokon, Tagum City<br>
                    <b>Age:</b> 20 years old<br>
                    <b>Gender:</b> Male<br>
                    <b>Birthdate: </b>05 May 1998</p> " ?>

                </div>
                
                <div class="section">
                    <div class="title"><img src="education.png" width="20" height="20">Education</div>

                    <?php echo "<p><b>BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY</b><br>
                    <i>University of Southeastern Philippines</i><br>
                    Bo. Obrero, Davao City</p>" ?>

                </div>
                
                <div class="section">
                    <div class="title"><img src="trainings.png" width="20" height="20">Trainings and Seminars</div>
                        <div class="content">

                        <?php echo "
                            <h2>University Leadership Enhancement Training</h2>
                            <h3>USeP | AUG 20 &raquo; 22, 2018</h3>
                            <ul>
                                <li>Maneuvering Student Service towards United and Student-centered Governance</li>
                                <li>ICLC - Vice Governor Representative</li>
                            </ul> " ?>

                        </div>
                        
                        
                        <div class="content">

                        <?php echo "
                            <h2>U:Hack - Tech Up Agri</h2>
                            <h3>Union Bank | AUG 25 &raquo; 27, 2018</h3>
                            <ul>
                                <li>Hackathon</li>
                                <li>Developers create new ideas and applications to help Agricultural sector</li>
                            </ul> " ?>
                        </div>
                    
                </div>
                
                <div class="section">
                    <div class="title"><img src="skills.png" width="20" height="20">Skills</div>

                    <?php echo "
                    <ul>
                        <li>Computer Programming</li>
                        <li>Multimedia</li>
                        <li>Project Management</li>
                    </ul> " ?>
                </div>
                
                
                
            </div>
        </div>
    </body>
</html>